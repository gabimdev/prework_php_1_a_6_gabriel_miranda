<?php
function countWord ($fileImported, $wordToCount) {
    $wordToCount = mb_strtolower ($wordToCount);
    $fileString = file_get_contents($fileImported);
    $fileString = mb_strtolower ($fileString,"UTF-8");
    $searchWord = substr_count($fileString, $wordToCount);
    echo "La palabra $wordToCount aparece $searchWord veces";
}

countWord ('./el_quijote.txt', "molino");
