<?php
function changeWords ($fileIn, $fileOut, $wordIn, $WordOut) {
    $newText = fopen ($fileOut, "w+");
    $textOpen = fopen ($fileIn, "r") or die ("No fue posible abrir el archivo $fileIn");
    while (!feof($textOpen)) {
        $line = fgets($textOpen);
        $line = str_ireplace ($wordIn, $WordOut, $line);
        file_put_contents($fileOut, $line, FILE_APPEND);
    }
}
fclose ($newText);
fclose ($textOpen);

changeWords ('./el_quijote.txt', 'quijote-modificado.txt', 'Quijote', 'Upgrade-Hub');