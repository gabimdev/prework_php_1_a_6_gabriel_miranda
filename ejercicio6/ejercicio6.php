<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Document</title>
</head>
<body>   

<p class="php_out"><?php
$email = $_POST["email"];
$password = $_POST["password"];
$passwordConfirm = $_POST["confirm_password"];
if ( ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
    echo "Por favor, compruebe la dirección de email introducida";
}else if (strlen($password) < 8) {
    echo "La contraseña es demasiado corta.<br>";
    echo "Por favor, introduzca al menos 8 caracteres";
} else if ( $password != $_POST["confirm_password"]) {
    echo "Las contraseñas no coinciden.<br>"; 
    echo "Por favor, inténtelo de nuevo";
} else {
    echo "Datos procesados correctamente";
}
?></p>

<button class="back"><a href="index.html" class="main_page"> Volver</a></button>

</body>
</html>

